#include <iostream>
#include <vector>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * List all files in direcotry
 * @param path Path to Directory
 * @return Vector of std::strings with all matches
 */
std::vector<std::string> ListDirectoryContent(std::string path)
{
    std::vector<std::string> files;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (path.c_str())) != NULL)
    {
        std::string filename;
        for (int i = 0; (ent = readdir (dir)) != NULL; i++)
        {
            filename = ent->d_name;
            if (filename.compare(".") == 0 || filename.compare("..") == 0)
            {
                continue;
            }
            files.push_back(filename);
        }
        closedir (dir);
    }
    return files;
}

void ShowFilesFromList(std::vector<std::string> content)
{
    for (size_t i = 0; i < content.size(); i++)
    {
        std::cout << "\t" << content[i].substr(0, content[i].size()-4) << std::endl;
    }
}


int main(int argc, char* argv[])
{
    if (argc <= 1) {
        std::cout << argv[0] << std::endl;
        std::cout << "Pfad zu Ordner als Parameter angeben!";
        return 0;
    } else {
        std::cout << argv[1];
    }
    std::string path = argv[1];
    std::cout << "Pfad zu Ordner angeben:" << std::endl;
    //std::getline(std::cin, path);
    if (path.substr(path.length()-1).compare("/") != 0)
    {
        path = path + "/";
    }
    std::cout << "Pfad: " << path << std::endl;
    std::vector<std::string> files(0);
    files = ListDirectoryContent(path);
    ShowFilesFromList(files);
    std::cout << "Found " << files.size() << " files." << std::endl << std::endl;
    std::cout << "Optionen:\r\n[e]:erase\r\n[r]:replace\r\n[-]:exit" << std::endl;
    char status;
    std::cin >> status;
    std::cin.ignore();
    switch (status)
    {
    case 'r':
    {
        //Replace
        std::cout << "Ersetze:" << std::endl;
        std::string repOldStr;
        std::getline(std::cin, repOldStr);
        std::cout << "Durch:" << std::endl;
        std::string repNewStr;
        std::getline(std::cin, repNewStr);
        int num;
        std::string number;
        std::cout << "Anzahl (Vorkommen pro Name):" << std::endl;
        std::getline(std::cin, number);
        num = atoi(number.c_str());

        std::string buffer;
        int symPos;
        int result;
        for (size_t i = 0; i < files.size(); i++)
        {
            buffer = files[i];
            for (int i = 0; i<num; i++)
            {
                symPos = buffer.substr(0, buffer.size()-4).find(repOldStr);
                if (symPos != -1)
                {
                    buffer.replace(symPos,repOldStr.size(), repNewStr);
                    //std::cout << "\t\t" << buffer << std::endl;
                }
            }
            result = rename((path+files[i]).c_str(), (path + buffer).c_str());
            files[i] = buffer;
            std::cout << "\t" << result << ":" << files[i] << std::endl;
        }
        break;
    }
    case 'e':
    {
        //Erase
        std::cout << "Entfernen:" << std::endl;
        std::string oldStr;
        std::getline(std::cin, oldStr);

        std::string buffer;
        int symPos;
        int result;
        for (size_t i = 0; i < files.size(); i++)
        {
            buffer = files[i];
            //std::cout << buffer << std::endl;
            symPos = buffer.substr(0, buffer.size()-4).find(oldStr);
            if (symPos != -1)
            {
                buffer.erase(symPos, oldStr.length());
            }
            result = rename((path+files[i]).c_str(), (path + buffer).c_str());
            files[i] = buffer;
            std::cout << "\t" << result << ":" << files[i] << std::endl;
        }
        break;
    }
    default:
    {
        std::cout << "Exit" << std::endl;
    }
    }

    //std::cin.get();
    return 0;
}
